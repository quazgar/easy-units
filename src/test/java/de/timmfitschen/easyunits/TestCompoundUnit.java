/*
 * This is a file from EasyUnits - A slim library for units
 * Copyright (C) 2018 Timm Fitschen (mail@timmfitschen.de)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for moredetails.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


package de.timmfitschen.easyunits;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.HashMap;

import org.junit.Test;

import de.timmfitschen.easyunits.conversion.Converter;
import de.timmfitschen.easyunits.conversion.Prefix;
import de.timmfitschen.easyunits.conversion.ProductConverter;

public class TestCompoundUnit {

	@Test
	public void testNormalTrue(){
		BaseUnit m = new BaseUnit("m");
		CompoundUnit m2 = m.times(m);
		assertTrue(m2.isNormal());
	}
	
	@Test
	public void testNormalFalse(){
		BaseUnit m = new BaseUnit("m");
		Unit km = m.divide(1000);
		CompoundUnit m_km = m.times(km);
		assertFalse(m_km.isNormal());
	}
	
	@Test
	public void testSquareKilometerToSquareMeter(){
		BaseUnit m = new BaseUnit("m");
		Unit km = m.divide(1000.0);
		assertEquals(1000.0, km.getConverter().convert(1).doubleValue(),0.000001);
		
		CompoundUnit km2 = km.times(km);
		assertEquals(m.times(m),km2.getNormalizedUnit());
		assertEquals(1000000.0, km2.getConverter().convert(1).doubleValue(),0.000001);
		
	}
	

	@Test(expected=NullPointerException.class)
	public void testInstantiationNull(){
		new CompoundUnit(null);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testInstantiationEmpty(){
		new CompoundUnit(new HashMap<>());
	}
	
	@Test(expected=UnsupportedOperationException.class)
	public void testInstantiation(){
		HashMap<Unit, Integer> map = new HashMap<>();
		map.put(new BaseUnit("m"), 1);
		CompoundUnit unit = new CompoundUnit(map);
		unit.getComponents().put(new BaseUnit("m"), 1);
	}
	
	@Test
	public void testToString(){
		CompoundUnit m2 = (CompoundUnit) new BaseUnit("m").power(2);
		assertEquals("[(m,2)]", m2.toString());
	}
	
	@Test
	public void testGetSymbol(){
		HashMap<Unit, Integer> map = new HashMap<>();
		map.put(new BaseUnit("m"), 1);
		CompoundUnit m = new CompoundUnit(map);
		assertEquals("m", m.getSymbol());
		
		map.put(new BaseUnit("m"), 2);
		CompoundUnit m2 = new CompoundUnit(map);
		assertNull(m2.getSymbol());
		
		map.put(new BaseUnit("s"), -1);
		CompoundUnit mps = new CompoundUnit(map);
		assertNull(mps.getSymbol());
		
	}
	
	@Test
	public void testGetConverterIdentity(){
		CompoundUnit m2 = (CompoundUnit) new BaseUnit("m").power(2);
		assertEquals(Converter.IDENTITY, m2.getConverter());
		
		CompoundUnit pm = (CompoundUnit) new BaseUnit("m").power(-1);
		assertEquals(Converter.IDENTITY, pm.getConverter());
	}
	
	@Test
	public void testGetConverterPrefixedUnit(){
		CompoundUnit cm2 = (CompoundUnit) new PrefixedUnit(new BaseUnit("m"), new Prefix("c", 10, -2)).power(2);
		assertEquals(1.0, cm2.getConverter().convert(10000).doubleValue(), 0.00001);
		assertEquals(new ProductConverter(1./10000), cm2.getConverter());
		
			CompoundUnit pcm = (CompoundUnit) new PrefixedUnit(new BaseUnit("m"), new Prefix("c", 10, -2)).power(-1);
		assertEquals(100, pcm.getConverter().convert(1).doubleValue(), 0.00001);
		assertEquals(new ProductConverter(100), pcm.getConverter());
	}
	
	@Test
	public void testGetNormalizedUnitIdentity(){
		CompoundUnit m2 = (CompoundUnit) new BaseUnit("m").power(2);
		assertEquals(m2, m2.getNormalizedUnit());
		
		CompoundUnit pm = (CompoundUnit) new BaseUnit("m").power(-1);
		assertEquals(pm, pm.getNormalizedUnit());
	}
	
	@Test
	public void testGetNormalizedUnitPrefixed(){
		CompoundUnit cm2 = (CompoundUnit) new PrefixedUnit(new BaseUnit("m"), new Prefix("c", 10, -2)).power(2);
		CompoundUnit m2 = (CompoundUnit) new BaseUnit("m").power(2);
		assertEquals(m2, cm2.getNormalizedUnit());

		
		CompoundUnit pcm = (CompoundUnit) new PrefixedUnit(new BaseUnit("m"), new Prefix("c", 10, -2)).power(-1);
		CompoundUnit pm = (CompoundUnit) new BaseUnit("m").power(-1);
		assertEquals(pm, pcm.getNormalizedUnit());
	}
	
	@Test
	public void testGetSignatureCompoundEqualsBase(){
		CompoundUnitFactory fac = new CompoundUnitFactory();
		fac.addComponent(new BaseUnit("m"), 1);
		assertEquals(new BaseUnit("m").getSignature(), fac.createUnit().getSignature());	
	}
	
	@Test
	public void testGetSignatureRatioIncommensurable(){
		CompoundUnit weightratio = (CompoundUnit) new BaseUnit("g").divide(new BaseUnit("g"));
		assertNotEquals(0, weightratio.getSignature());
		
		CompoundUnit volratio = (CompoundUnit) new BaseUnit("l").divide(new BaseUnit("l"));
		assertNotEquals(weightratio.getSignature(), volratio.getSignature());
	}
	
}
