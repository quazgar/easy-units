/*
 * This is a file from EasyUnits - A slim library for units
 * Copyright (C) 2018 Timm Fitschen (mail@timmfitschen.de)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for moredetails.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


package de.timmfitschen.easyunits;

import static org.junit.Assert.*;

import java.util.HashMap;

import org.junit.Test;

import de.timmfitschen.easyunits.conversion.Prefix;

class TestFac extends DefaultSystemOfUnitsFactory {
	@Override
	public void addPrefixedUnit(Prefix prefix, Unit unit) throws SystemOfUnitsException {
		super.addPrefixedUnit(prefix, unit);
	}
	
	@Override
	public HashMap<String, Unit> getAllunits() {
		return super.getAllunits();
	}
	
	@Override
	public void addUnitWithAllNames(Unit unit) throws SystemOfUnitsException {
		super.addUnitWithAllNames(unit);
	}
}

public class TestDefaultSystemOfUnitsFactory {

	
	@Test
	public void testInstantiation(){
		new DefaultSystemOfUnitsFactory();
	}
	
	@Test
	public void testGetUnitNull(){
		DefaultSystemOfUnitsFactory fac = new DefaultSystemOfUnitsFactory();
		assertNull(fac.getUnit("m"));
	}
	
	@Test(expected=UnsupportedOperationException.class)
	public void testAddDerivedUnitCalls() throws SystemOfUnitsException{
		DefaultSystemOfUnitsFactory fac = new DefaultSystemOfUnitsFactory(){
			@Override
			protected void addUnitWithAllNames(Unit unit) throws SystemOfUnitsException {
				throw new UnsupportedOperationException();
			};
		};
		fac.add((DerivedUnit) null);
	}
	
	
	@Test(expected=UnsupportedOperationException.class)
	public void testAddBaseUnitCalls() throws SystemOfUnitsException{
		DefaultSystemOfUnitsFactory fac = new DefaultSystemOfUnitsFactory(){
			@Override
			protected void addUnitWithAllNames(Unit unit) throws SystemOfUnitsException {
				throw new UnsupportedOperationException();
			};
		};
		fac.add((BaseUnit) null);
	}
	
	@Test(expected=NullPointerException.class)
	public void testAddBaseUnitNull() throws SystemOfUnitsException{
		new	DefaultSystemOfUnitsFactory().add((BaseUnit) null);
	}
	
	@Test(expected=NullPointerException.class)
	public void testAddDerivedUnitNull() throws SystemOfUnitsException{
		new	DefaultSystemOfUnitsFactory().add((DerivedUnit) null);
	}
	
	@Test(expected=NullPointerException.class)
	public void testAddPrefixNull() throws SystemOfUnitsException{
		new	DefaultSystemOfUnitsFactory().add((Prefix) null);
	}
	
	@Test(expected=NullPointerException.class)
	public void testAddPrefixedUnitNullPrefix() throws SystemOfUnitsException{
		new TestFac().addPrefixedUnit(null, new BaseUnit("m"));
	}
	
	@Test(expected=NullPointerException.class)
	public void testAddPrefixedUnitNullUnit() throws SystemOfUnitsException{
		new TestFac().addPrefixedUnit(new Prefix("k", 10, 3), null);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testAddPrefixedUnitNonPrefixable() throws SystemOfUnitsException{
		TestFac fac = new TestFac();
		assertEquals(1,fac.getAllunits().size());
		fac.addUnitWithAllNames(new BaseUnit("m").setPrefixableBy("none"));
		assertEquals(2,fac.getAllunits().size());
		fac.addPrefixedUnit(new Prefix("k", 10, 3), new BaseUnit("m").setPrefixableBy("none"));
	}
	
	@Test
	public void testAddBaseUnitNotPrefixableFirstUnit() throws SystemOfUnitsException{
		TestFac fac = new TestFac();
		assertEquals(1,fac.getAllunits().size());
		fac.addUnitWithAllNames(new BaseUnit("m").setPrefixableBy("none"));
		assertEquals(2, fac.getAllunits().size());
	}
	
	@Test(expected=SystemOfUnitsException.class)
	public void testAddBaseUnitExisting() throws SystemOfUnitsException{
		TestFac fac = new TestFac();
		assertEquals(1,fac.getAllunits().size());
		fac.addUnitWithAllNames(new BaseUnit("m").setPrefixableBy("none"));
		assertEquals(2, fac.getAllunits().size());
		fac.addUnitWithAllNames(new BaseUnit("m"));
	}
	
	@Test
	public void testAddBaseUnitNotPrefixableFirstUnitWithNames() throws SystemOfUnitsException{
		TestFac fac = new TestFac();
		assertEquals(1,fac.getAllunits().size());
		fac.addUnitWithAllNames((BaseUnit) new BaseUnit("m").setPrefixableBy("none").addName("metre"));
		assertEquals(3, fac.getAllunits().size());
		assertEquals(new BaseUnit("m"), fac.getUnit("metre"));
	}
	
	@Test
	public void testAddBaseUnitNotPrefixableFirstUnitWithSameName() throws SystemOfUnitsException{
		TestFac fac = new TestFac();
		assertEquals(1,fac.getAllunits().size());
		fac.addUnitWithAllNames((BaseUnit) new BaseUnit("m").setPrefixableBy("none").addName("m"));
		assertEquals(2, fac.getAllunits().size());
	}
	
	@Test(expected=SystemOfUnitsException.class)
	public void testAddBaseUnitExistingName() throws SystemOfUnitsException{
		TestFac fac = new TestFac();
		assertEquals(1,fac.getAllunits().size());
		fac.addUnitWithAllNames(new BaseUnit("m").setPrefixableBy("none"));
		assertEquals(2, fac.getAllunits().size());
		fac.addUnitWithAllNames((Unit) new BaseUnit("metre").addName("m"));
	}
	
	@Test(expected=SystemOfUnitsException.class)
	public void testAddPrefixedUnitUndefinedUnit() throws SystemOfUnitsException{
		TestFac fac = new TestFac();
		assertEquals(1,fac.getAllunits().size());
		fac.addPrefixedUnit(new Prefix("k", 10, 3), new BaseUnit("m"));
	}
	
	@Test
	public void testAddPrefixUnit() throws SystemOfUnitsException{
		TestFac fac = new TestFac();
		assertEquals(1,fac.getAllunits().size());
		fac.addUnitWithAllNames(new BaseUnit("m").setPrefixableBy("none"));
		assertEquals(2,fac.getAllunits().size());
		fac.addPrefixedUnit(new Prefix("k", 10, 3), new BaseUnit("m"));
		assertEquals(3,fac.getAllunits().size());
	}
	
	@Test(expected=SystemOfUnitsException.class)
	public void testAddPrefixUnitSymbolCollision() throws SystemOfUnitsException{
		TestFac fac = new TestFac();
		assertEquals(1,fac.getAllunits().size());
		fac.addUnitWithAllNames(new BaseUnit("m").setPrefixableBy("none"));
		fac.addUnitWithAllNames(new BaseUnit("km").setPrefixableBy("none"));
		assertEquals(3,fac.getAllunits().size());
		fac.addPrefixedUnit(new Prefix("k", 10, 3), new BaseUnit("m"));
	}
	
	@Test
	public void testAddPrefixedUnitWithNames() throws SystemOfUnitsException{
		TestFac fac = new TestFac();
		assertEquals(1,fac.getAllunits().size());
		fac.addUnitWithAllNames((Unit) new BaseUnit("m").setPrefixableBy("none").addName("metre"));
		assertEquals(3,fac.getAllunits().size());
		fac.addPrefixedUnit(new Prefix("k", 10, 3), (Unit) new BaseUnit("m").addName("metre"));
		assertEquals(5,fac.getAllunits().size());
	}
	
	@Test(expected=SystemOfUnitsException.class)
	public void testAddPrefixedUnitNamesCollision() throws SystemOfUnitsException{
		TestFac fac = new TestFac();
		assertEquals(1,fac.getAllunits().size());
		fac.addUnitWithAllNames((Unit) new BaseUnit("m").setPrefixableBy("none").addName("kmetre"));
		assertEquals(3,fac.getAllunits().size());
		fac.addPrefixedUnit(new Prefix("k", 10, 3), (Unit) new BaseUnit("m").addName("metre"));
	}
	
	@Test
	public void testAddPrefixedUnitWithPrefixName() throws SystemOfUnitsException{
		TestFac fac = new TestFac();
		assertEquals(1,fac.getAllunits().size());
		fac.addUnitWithAllNames((Unit) new BaseUnit("m").setPrefixableBy("none"));
		assertEquals(2,fac.getAllunits().size());
		fac.addPrefixedUnit((Prefix) new Prefix("k", 10, 3).addName("kilo"), (Unit) new BaseUnit("m"));
		assertEquals(4,fac.getAllunits().size()); // 1, m, km, kilom
	}
	
	@Test
	public void testAddPrefixNoPrefixableUnit() throws SystemOfUnitsException{
		TestFac fac = new TestFac();
		assertEquals(1,fac.getAllunits().size());
		fac.add(new Prefix("k", 10, 3));
		assertEquals(1,fac.getAllunits().size());
		fac.addUnitWithAllNames(new BaseUnit("m"));
		assertEquals(3,fac.getAllunits().size());
	}
	
	@Test
	public void testAddPrefixWithPrefixableUnit() throws SystemOfUnitsException{
		TestFac fac = new TestFac();
		assertEquals(1,fac.getAllunits().size());
		fac.addUnitWithAllNames(new BaseUnit("m"));
		assertEquals(2,fac.getAllunits().size());
		fac.add(new Prefix("k", 10, 3));
		assertEquals(3,fac.getAllunits().size());
	}
	
	@Test
	public void testAddBaseUnit() throws SystemOfUnitsException{
		TestFac fac = new TestFac();
		assertEquals(1,fac.getAllunits().size());
		fac.add(new BaseUnit("m"));
		assertEquals(2,fac.getAllunits().size());
	}
	
	@Test
	public void testAddDerivedUnitWithUndefinedBaseUnit() throws SystemOfUnitsException{
		TestFac fac = new TestFac();
		assertEquals(1,fac.getAllunits().size());
		fac.add(new DerivedUnit("n", new BaseUnit("m")));
		assertEquals(2,fac.getAllunits().size());
	}
	
	@Test
	public void testCreate(){
		TestFac fac = new TestFac();
		SystemOfUnits sou = fac.create(); 
		assertNotNull(sou);
	}
}
