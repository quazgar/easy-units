/*
 * This is a file from EasyUnits - A slim library for units
 * Copyright (C) 2018 Timm Fitschen (mail@timmfitschen.de)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for moredetails.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


package de.timmfitschen.easyunits.conversion;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class TestAbstractConverter {
	
	public AbstractConverter converter = new AbstractConverter() {
		
		@Override
		public long getSignature() {
			return 0;
		}
		
		@Override
		public Number convert(Number value) {
			return 0;
		}
		
		@Override
		protected Converter concatenateNonIdentityConverter(Converter c) {
			return new ProductConverter(1/2);
		}
	};
	
	@Test
	public void testConcatenate() {
		assertEquals(converter, converter.concatenate(Converter.IDENTITY));
		assertEquals(converter, converter.concatenate(null));
		assertEquals(new ProductConverter(1/2),converter.concatenate(new OffsetConverter(1.0)));
	}
	
}
