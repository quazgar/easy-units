/*
 * This is a file from EasyUnits - A slim library for units
 * Copyright (C) 2018 Timm Fitschen (mail@timmfitschen.de)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for moredetails.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


package de.timmfitschen.easyunits;

import static org.junit.Assert.*;

import org.junit.Test;

public class TestCompoundUnitFactory {

	@Test(expected = IllegalArgumentException.class)
	public void testCreateUnit() {
		CompoundUnitFactory fac = new CompoundUnitFactory();
		fac.createUnit();
	}
	
	@Test(expected = NullPointerException.class)
	public void testAddComponentNull(){
		CompoundUnitFactory fac = new CompoundUnitFactory();
		fac.addComponent((Unit) null, 1);
	}
	
	@Test(expected = NullPointerException.class)
	public void testAddComponentNull2(){
		CompoundUnitFactory fac = new CompoundUnitFactory();
		fac.addComponent((CompoundUnit) null, 1);
	}
	
	@Test
	public void testAddComponent(){
		BaseUnit m = new BaseUnit("m");
		CompoundUnitFactory fac = new CompoundUnitFactory();
		fac.addComponent(m, 1);
		CompoundUnit m2 = fac.createUnit();
		assertEquals(m2,m);
		
		fac.addComponent(m, 1);
		assertEquals(m.power(2),fac.createUnit());
	}
	
	@Test
	public void testAddComponent2(){
		BaseUnit m = new BaseUnit("m");
		CompoundUnitFactory fac = new CompoundUnitFactory();
		fac.addComponent(m, 1);
		CompoundUnit m2 = fac.createUnit();
		assertEquals(m2,m);
		
		fac.addComponent(m2, 1);
		assertEquals(m.power(2),fac.createUnit());
	}
	
	@Test
	public void testAddComponent3(){
		BaseUnit m = new BaseUnit("m");
		CompoundUnitFactory fac = new CompoundUnitFactory();
		fac.addComponent(m, 1);
		CompoundUnit m2 = fac.createUnit();
		assertEquals(m2,m);
		
		fac.addComponent((Unit) m2, 1);
		assertEquals(m.power(2),fac.createUnit());
	}
	
	
}
