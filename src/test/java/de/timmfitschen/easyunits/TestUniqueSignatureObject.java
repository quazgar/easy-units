/*
 * This is a file from EasyUnits - A slim library for units
 * Copyright (C) 2018 Timm Fitschen (mail@timmfitschen.de)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for moredetails.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


package de.timmfitschen.easyunits;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import org.junit.Test;

public class TestUniqueSignatureObject {
	
	private static class TestClass extends UniqueSignatureObject {
		
		private long signature;

		public TestClass(long signature) {
			this.signature = signature;
		}
		
		@Override
		public long getSignature() {
			return signature;
		}
	};
	
	
	
	@Test
	public void testEquals() {
		assertEquals(new TestClass(0), new TestClass(0));
		assertNotEquals(new TestClass(0), new TestClass(1));
		assertNotEquals(new TestClass(0), "asdf");
	}
	
}
