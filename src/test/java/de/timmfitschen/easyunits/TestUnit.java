/*
 * This is a file from EasyUnits - A slim library for units
 * Copyright (C) 2018 Timm Fitschen (mail@timmfitschen.de)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for moredetails.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


package de.timmfitschen.easyunits;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import de.timmfitschen.easyunits.conversion.OffsetConverter;
import de.timmfitschen.easyunits.conversion.Converter;
import de.timmfitschen.easyunits.conversion.LinearConverter;
import de.timmfitschen.easyunits.conversion.Prefix;
import de.timmfitschen.easyunits.conversion.ProductConverter;

public class TestUnit {

	@Test
	public void testAddCallsTranform() {
		double offset = -273.15;
		Unit kelvin = new BaseUnit("K") {
			@Override
			public Unit transform(Converter c) {
				assertEquals(c, new OffsetConverter(-offset));
				return super.transform(c);
			}
		};
		Unit celsius = kelvin.add(offset);
		Converter celsius2kelvin = celsius.getConverter();
		assertEquals(273.15, celsius2kelvin.convert(0).doubleValue(), 0.00001);
	}

	@Test
	public void testTimesDoubleCallsTransform() {
		double prefix = 100.0;
		Unit metre = new BaseUnit("m") {
			@Override
			public Unit transform(Converter c) {
				assertEquals(c, new ProductConverter(1/prefix));
				return super.transform(c);
			}
		};
		Unit cm = metre.times(prefix);
		assertEquals(1.0, cm.getConverter().convert(100).doubleValue(), 0.0001);
	}
	
	@Test
	public void testTimesIntCallsTransform() {
		int prefix = 1000;
		Unit metre = new BaseUnit("m") {
			@Override
			public Unit transform(Converter c) {
				assertEquals(c, new ProductConverter(1./1000));
				return super.transform(c);
			}
		};
		Unit mm = metre.times(prefix);
		assertEquals(1.0, mm.getConverter().convert(1000).doubleValue(), 0.0001);
	}

	@Test
	public void testDivideIntCallsTransform() {
		int spread = 3;
		Unit ft = new BaseUnit("ft") {
			@Override
			public Unit transform(Converter c) {
				assertEquals(c, new ProductConverter(spread));
				return super.transform(c);
			}
		};
		Unit yards = ft.divide(spread);
		Converter yd2ft = yards.getConverter();
		assertEquals(3.0, yd2ft.convert(1).doubleValue(), 0.0001);
	}

	@Test
	public void testDivideDoubleCallsTransform() {
		double spread = 0.3048;
		Unit metre = new BaseUnit("m") {
			@Override
			public Unit transform(Converter c) {
				assertEquals(c, new ProductConverter(3048./10000));
				return super.transform(c);
			}
		};
		Unit ft = metre.divide(spread);
		Converter ft2m = ft.getConverter();
		assertEquals(0.3048, ft2m.convert(1).doubleValue(), 0.00001);
	}

	@Test
	public void testTimesUnit() {
		Unit metre = new BaseUnit("m");
		Symboled sqmeter = metre.times(metre);
		assertTrue(sqmeter instanceof CompoundUnit);
	}

	@Test
	public void testDevideUnit() {
		Unit gram = new BaseUnit("g");
		Symboled concentration = gram.divide(gram);
		assertTrue(concentration instanceof CompoundUnit);
	}

	@Test
	public void testMultiTransformation() {
		BaseUnit kelvin = new BaseUnit("K");
		Unit celsius = kelvin.add(-273.15);
		Unit fahrenheit = celsius.times(1.8).add(32);
		Unit fahrenheit2 = celsius.times(18).divide(10).add(32);
		Converter f2K = fahrenheit.getConverter();
		Converter f2K2 = fahrenheit2.getConverter();
		assertEquals(f2K2, f2K);
		assertEquals(new LinearConverter(-32, 10./18, 273.15), f2K);
		assertEquals(0.0, f2K.convert(-459.67).doubleValue(), 0.00001);
		assertEquals(255.37222, f2K.convert(0).doubleValue(), 0.00001);
		assertEquals(273.15, f2K.convert(32).doubleValue(), 0.00001);
		assertEquals(310.92777, f2K.convert(100).doubleValue(), 0.00001);
	}
	
	@Test
	public void testPrefix(){
		BaseUnit m = new BaseUnit("m");
		Prefix kilo = new Prefix("k", 10, 3);
		Unit km = m.transform(kilo.getInversion());
		assertEquals(1000.0, km.getConverter().convert(1.0).doubleValue(), 0.00001);
	}
	
	@Test
	public void testTransformNull(){
		BaseUnit m = new BaseUnit("m");
		assertEquals(m, m.transform(null));
	}
	
	@Test
	public void testTransformIdentity(){
		BaseUnit m = new BaseUnit("m");
		assertEquals(m, m.transform(Converter.IDENTITY));
	}
	
	@Test
	public void testIsNormal(){
		BaseUnit m = new BaseUnit("m");
		assertTrue(m.isNormal());
		assertFalse(m.divide(2).isNormal());
	}
	
	@Test
	public void testPowerReturnsCompound(){
		BaseUnit m = new BaseUnit("m");
		assertEquals(new CompoundUnitFactory().addComponent(m, 2).createUnit(), m.power(2));
	}
	
	@Test
	public void testSetPrefixable(){
		BaseUnit m = new BaseUnit("K");
		assertEquals("all", m.getPrefixableBy());
		assertEquals(m, m.setPrefixableBy("none"));
		assertEquals("none", m.getPrefixableBy());
	}
}
