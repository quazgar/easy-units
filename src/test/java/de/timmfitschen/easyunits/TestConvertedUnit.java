/*
 * This is a file from EasyUnits - A slim library for units
 * Copyright (C) 2018 Timm Fitschen (mail@timmfitschen.de)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for moredetails.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


package de.timmfitschen.easyunits;

import static org.junit.Assert.*;

import org.junit.Test;

import de.timmfitschen.easyunits.conversion.LinearConverter;
import de.timmfitschen.easyunits.conversion.ProductConverter;

public class TestConvertedUnit {
	
	ConvertedUnit ft = new ConvertedUnit(new BaseUnit("m"), new LinearConverter(0, 3048./1000, 0));

	@Test
	public void testInstantiation(){
		assertNotNull(ft);
	}
	
	@Test
	public void testGetConverter(){
		assertEquals(new ProductConverter(3048./1000), ft.getConverter());
	}
	
	@Test
	public void testGetNormalizedUnit(){
		assertEquals(new BaseUnit("m"), ft.getNormalizedUnit());
	}
	
	@Test
	public void testGetSymbol(){
		assertEquals("mLinCon(0.0,3.048,0.0)",ft.getSymbol());
	}
	
	@Test
	public void testGetSignature(){
		assertNotEquals(0, ft.getSignature());
	}
}
