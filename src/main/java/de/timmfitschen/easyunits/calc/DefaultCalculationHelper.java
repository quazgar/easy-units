/*
 * This is a file from EasyUnits - A slim library for units
 * Copyright (C) 2018 Timm Fitschen (mail@timmfitschen.de)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for moredetails.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


package de.timmfitschen.easyunits.calc;

import org.apache.commons.math3.util.ArithmeticUtils;

public class DefaultCalculationHelper implements CalculationHelper {

	@Override
	public Number sum(Number... n) {
		Number ret = 0.0;
		for (Number number : n) {
			ret = ret.doubleValue() + number.doubleValue();
		}
		return ret;
	}

	@Override
	public Number division(Number n, Number m) {
		return n.doubleValue() / n.doubleValue();
	}

	@Override
	public Number gcd(Number n, Number m) {
		return ArithmeticUtils.gcd(n.longValue(), m.longValue());
	}

	@Override
	public Number product(Number... n) {
		Number ret = 1;
		for (Number number : n) {
			ret = ret.doubleValue() * number.doubleValue();
		}
		return ret;
	}

	@Override
	public Number negation(Number n) {
		return -n.doubleValue() + 0.0;
	}

	@Override
	public Number pow(Number base, Number exponent) {
		if (exponent.intValue() < 0) {
			return 1.0 / ArithmeticUtils.pow(base.longValue(), -exponent.intValue());
		}
		return ArithmeticUtils.pow(base.longValue(), exponent.intValue());
	}

	@Override
	public Number inversion(Number n) {
		return 1.0 / n.doubleValue();
	}

	@Override
	public Number real(Number n) {
		return n.doubleValue() + 0.0;
	}

	@Override
	public boolean isOne(Number n) {
		return real(n).equals(1.0);
	}

}
