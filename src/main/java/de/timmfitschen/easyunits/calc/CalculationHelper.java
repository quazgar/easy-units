/*
 * This is a file from EasyUnits - A slim library for units
 * Copyright (C) 2018 Timm Fitschen (mail@timmfitschen.de)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for moredetails.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


package de.timmfitschen.easyunits.calc;

class CalculationHelperInstanceWrapper {
	public static CalculationHelper INSTANCE = new DefaultCalculationHelper();
}

public interface CalculationHelper {
	
	public static CalculationHelper getCalculationHelper(){
		return CalculationHelperInstanceWrapper.INSTANCE;
	}

	public static void setCalculationHelper(CalculationHelper helper){
		CalculationHelperInstanceWrapper.INSTANCE = helper;
	}

	public Number sum(Number... n);

	/**
	 * Calculate the division n/m.
	 * @param n
	 * @param m
	 * @return
	 */
	public Number division(Number n, Number m);

	/**
	 * Calculate the greatest common divisor of two numbers.
	 * @param b_dividend
	 * @param b_divisor
	 * @return
	 */
	public Number gcd(Number b_dividend, Number b_divisor);

	public Number product(Number... n);

	public Number negation(Number n);

	public Number pow(Number base, Number i);

	public Number inversion(Number b);

	public Number real(Number a);

	public boolean isOne(Number b);
	
}
