/*
 * This is a file from EasyUnits - A slim library for units
 * Copyright (C) 2018 Timm Fitschen (mail@timmfitschen.de)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for moredetails.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


package de.timmfitschen.easyunits;

public abstract class UniqueSignatureObject implements Signed {

	/**
	 * Return true iff `obj` is {@link UniqueSignatureObject} and has the same
	 * signature as this {@link UniqueSignatureObject}.
	 */
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof UniqueSignatureObject) {
			UniqueSignatureObject that = (UniqueSignatureObject) obj;
			return that.getSignature() == this.getSignature();
		}
		return false;
	}

}
