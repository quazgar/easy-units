/*
 * This is a file from EasyUnits - A slim library for units
 * Copyright (C) 2018 Timm Fitschen (mail@timmfitschen.de)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for moredetails.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


package de.timmfitschen.easyunits;

import de.timmfitschen.easyunits.conversion.Converter;

public class ConvertedUnit extends Unit {

    private Unit unit;
    private Converter converter;

    public ConvertedUnit(Unit unit, Converter converter) {
    	super();
        this.unit = unit;
        this.converter = converter;
    }

    @Override
		public String getSymbol() {
			return unit.getSymbol() + converter.toString();
		}

    @Override
    public Unit getNormalizedUnit() {
        return unit.getNormalizedUnit();
    }

    @Override
    public Converter getConverter() {
        return converter;
    }

    @Override
    public long getSignature() {
        return unit.getSignature() + converter.getSignature();
    }

}
