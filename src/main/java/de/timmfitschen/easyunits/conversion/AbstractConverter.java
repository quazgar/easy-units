/*
 * This is a file from EasyUnits - A slim library for units
 * Copyright (C) 2018 Timm Fitschen (mail@timmfitschen.de)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for moredetails.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


package de.timmfitschen.easyunits.conversion;

import de.timmfitschen.easyunits.UniqueSignatureObject;

public abstract class AbstractConverter extends UniqueSignatureObject implements Converter {

	/**
	 * Return a converter which is equivalent to applying the `nextConverter` on the
	 * result of a conversion with this converter. If `nextConverter` is null or the
	 * identity conversion, this converter is returned without instantiating a new
	 * converter. Otherwise, a new converter is instantiated while this converter is
	 * not altered.
	 * 
	 * Usually, this method should not be overridden. Override the abstract method
	 * `concatenateNonIdentityConverter` instead.
	 */
	@Override
	public Converter concatenate(Converter nextConverter) {
		if (nextConverter == null || Converter.IDENTITY.equals(nextConverter)) {
			return this;
		}
		return concatenateNonIdentityConverter(nextConverter);
	}

	/**
	 * Return a new Converter by concatenating another converter to this converter.
	 * The new converter is equivalent to applying the `nextConverter` on the result
	 * of a conversion with this converter.
	 * 
	 * @param nextConverter
	 * @return a new converter
	 */
	protected abstract Converter concatenateNonIdentityConverter(Converter nextConverter);

}
