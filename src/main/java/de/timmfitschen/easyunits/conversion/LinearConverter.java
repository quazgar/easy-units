/*
 * This is a file from EasyUnits - A slim library for units
 * Copyright (C) 2018 Timm Fitschen (mail@timmfitschen.de)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for moredetails.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


package de.timmfitschen.easyunits.conversion;

import de.timmfitschen.easyunits.Invertible;
import de.timmfitschen.easyunits.calc.CalculationHelper;

public class LinearConverter extends AbstractConverter implements Invertible {

	private final Number a;
	private final Number b;
	private final Number c;
	private CalculationHelper helper = CalculationHelper.getCalculationHelper();
	
	public CalculationHelper getHelper() {
		return helper;
	}
	
	public void setHelper(CalculationHelper helper) {
		this.helper = helper;
	}

	public LinearConverter(Number a, Number b, Number c) {
		this.b = helper.real(b);
		if(helper.isOne(this.b)){
			this.a = helper.sum(a,c);
			this.c = helper.real(0);
		} else {
			this.a = helper.real(a);
			this.c = helper.real(c);
		}
	}

	@Override
	protected Converter concatenateNonIdentityConverter(Converter converter) {
		if (converter instanceof LinearConverter) {
			LinearConverter that = (LinearConverter) converter;
			
			Number b = helper.product(this.b, that.b);
			Number c = helper.sum(helper.product(this.c, that.b),
					helper.product(that.getA(), that.b), that.c);
			return new LinearConverter(this.getA(), b, c);
		}
		throw new UnsupportedOperationException("Non-linear conversions are not supported yet.");
	}

	@Override
	public long getSignature() {
		if (b.equals(1) && getA().equals(0) && c.equals(0)) {
			return 0;
		}
		StringBuilder sb = new StringBuilder("LinearConverter");
		sb.append(getA());
		sb.append(',');
		sb.append(b);
		sb.append(',');
		sb.append(c);
		return sb.toString().hashCode();
	}

	@Override
	public LinearConverter getInversion() {
		return new LinearConverter(helper.negation(c), helper.inversion(b), helper.negation(getA()));
	}

	@Override
	public Number convert(Number value) {
		return helper.sum(helper.product(helper.sum(value,getA()), b), c);
	}

	@Override
	public String toString() {
		return "LinCon(" + getA() + "," + b + "," + c + ")";
	}

	public Number getA() {
		return a;
	}

	public Number getB() {
		return b;
	}
	
	public Number getC() {
		return c;
	}
}
